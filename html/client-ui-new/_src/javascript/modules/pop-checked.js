ns('pop-checked', function() {

  function getAllRadios($radio) {
    var $wrap = $radio.parents('form');
    if ($wrap.length === 0) {
      $wrap = $(document);
    }
    return $wrap.find('input[type="' + $radio.attr('type') + '"][name="' + $radio.attr('name') + '"]');
  }

  $('.js-pop-checked').each(function() {
    var $input = $(this);
    var $target = $input.parents('.js-pop-checked__target');

    function update() {
      $target.toggleClass('is-checked', $input.prop('checked'));
    }

    update();
    getAllRadios($input).on('change', update);
  });

});
