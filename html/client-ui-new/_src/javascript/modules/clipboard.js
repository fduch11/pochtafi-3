ns('clipboard', function(exports) {

  exports.init = function() {

    var initd = 'clipboard-initialised';

    $('[data-clipboard-text]').each(function() {
      var $btn = $(this)

      if (!$btn.data(initd)) {
        $btn.data(initd, true);

        var mainText = $btn.text();
        var altText = $btn.data('alt-text');
        var timeoutId = null;
        $btn.css({visibility: 'hidden'});
        try {
          var client = new ZeroClipboard(this);
          client.on("ready", function(event) {
            $btn.css({visibility: 'visible'});
            client.on("aftercopy", function(event) {
              $btn.blur().text(altText);
              if (timeoutId !== null) {
                clearTimeout(timeoutId);
                timeoutId = null;
              }
              timeoutId = setTimeout(function() {
                $btn.text(mainText);
                timeoutId = null;
              }, 2000);
            });
          });
        } catch (e) {
          console && console.error && console.error('На этой странице должен быть подключен файл:\nvendor-packages/zeroclipboard/ZeroClipboard.min.js')
        }
      }

    });

  };

});
