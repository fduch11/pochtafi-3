<?php

require_once('lib/tulli/CustomsNotifyCallback.php');

class Iwings_Pochta_ServicesController extends Mage_Core_Controller_Front_Action
{
    /**
     * Receives Customs Notifications
     *
     */
    public function dmeAction()
    {
        ini_set("soap.wsdl_cache_enabled", "0"); // turn WSDL cache off

        $options = array('classmap' => array('RequestHeader' => "NotifyRequestHeader"));

        $server = new SoapServer('lib/tulli/schemas/dme/NotificationService.wsdl', $options);
        $server->setClass("CustomsNotifyCallback");
        $server->handle();
    }

}
