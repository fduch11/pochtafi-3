<?php
require_once Mage::getModuleDir('controllers', 'Iwings_Pochta').DS.'BaseController.php';
require_once Mage::getModuleDir('controllers', 'Iwings_Pochta').DS.'PaymentsController.php';

class Iwings_Pochta_KioskController extends Iwings_Pochta_BaseController //Mage_Core_Controller_Front_Action
{

    public function choosePlacePostAction() {
        $currentPlace = $_POST['place'];
        Mage::getSingleton('core/session')->setSelectedStoreId($currentPlace);

        $this->_redirect('/');
    }

    public function searchPostAction()
    {
        Mage::getSingleton('core/session')->unsKiosk();
        Mage::getSingleton('core/session')->setKiosk(array());

		$suite = trim($_POST['search']);

        if($suite) {
			$customer = Mage::getModel('customer/customer');
			$customer = $customer->getCollection()->addAttributeToFilter('suite', $suite)->getFirstItem();
			if ($customer && $customer->getId()) {

                /** @var $helper Iwings_Pochta_Helper_Data */
                $helper = Mage::helper('pochta');

                $filters = array(
                    0 => array('box_store' => Mage::getSingleton('core/session')->getSelectedStoreId()),
//                    1 => array('state_physical' => Mage::registry('state_physical_good')),
                    2 => array('state_control' => Mage::registry('state_control_normal')),
                    3 => array('state_payment' => array( array('eq' => Mage::registry('state_payment_ready_for_payment')), array('eq' => Mage::registry('state_payment_paid')) ))
                );
                if ($helper->getParcelCollectionSize($filters, $customer->getId())) {

                    $kiosk = Mage::getSingleton('core/session')->getKiosk();
                    $kiosk['search'] = $suite;
                    $kiosk['parcels'] = $helper->getParcelCollection($filters, $customer->getId())->getAllIds();
                    Mage::getSingleton('core/session')->setKiosk($kiosk);

                    $this->_redirect('pochta/kiosk/parcels');

                    return;
                }
			}
		}

        $this->_redirect('', array('_query' => array('error'=>true)));
    }

    public function parcelsAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function parcelsPostAction() {

        $kiosk = Mage::getSingleton('core/session')->getKiosk();
        $kiosk['parcels'] = $_POST['parcel_id'];

        $total = 0.0;
        $declaration_required = false;
        foreach ($kiosk['parcels'] as $parcelId) {
            $parcel = Mage::getModel('catalog/product')->load($parcelId);
            if (!Mage::helper('pochta')->isPaid($parcel)) {
                $total += Mage::helper('pochta')->getRefundableAmount($parcel) + Mage::helper('pochta')->getNonRefundableAmount($parcel);
            }
            if ($parcel->getBoxOrigin() != Mage::registry('box_origin_european')) {
                $declaration_required = true;
            }
        }

        $kiosk['total'] = $total;
        $kiosk['declaration_required'] = $declaration_required;
        Mage::getSingleton('core/session')->setKiosk($kiosk);


        $this->_redirect('pochta/kiosk/billing');
    }

    public function billingAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function billingPostAction() {

        $kiosk = Mage::getSingleton('core/session')->getKiosk();

        $kiosk['carrier_name_first'] = $this->getRequest()->getParam('carrier_name_first');
        $kiosk['carrier_name_last'] = $this->getRequest()->getParam('carrier_name_last');
        $kiosk['consignee_address_line'] = $this->getRequest()->getParam('consignee_address_line');
        $kiosk['consignee_address_city'] = $this->getRequest()->getParam('consignee_address_city');
        $kiosk['consignee_address_country'] = $this->getRequest()->getParam('consignee_address_country');
        $kiosk['consignee_address_postcode'] = $this->getRequest()->getParam('consignee_address_postcode');

        $total = $kiosk['total'];
        $declaration_required = $kiosk['declaration_required'];

        Mage::getSingleton('core/session')->setKiosk($kiosk);

        if ($total > 0) {
            //$this->_redirect('pochta/kiosk/payment');
            $this->_redirect('pochta/kiosk/payPaypal');
        } else if ($declaration_required) {
            $this->_redirect('pochta/kiosk/transport');
        } else {
            return $this->processPostAction();
        }

    }

    public function payPaypalAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function paymentAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function paymentDoneAction()
    {
        $kiosk = Mage::getSingleton('core/session')->getKiosk();

        $parcels = $this->getRequest()->getParam('custom');
        if (!$parcels) {
            $parcels = $kiosk['parcels'];
        } else {
            $parcels = explode(',', $parcels);
        }

        foreach ($parcels as $parcelId) {
            $parcel = Mage::getModel('catalog/product')->load($parcelId);
            $parcel->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);

            $parcel->setStatePayment(Mage::registry('state_payment_paid'));
            $parcel->save();
        }

        $payment_transactions = array();
        $payment_transactions[$this->getRequest()->getParam('txn_id')] = Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE; // TODO: HACK
        //$payment_transactions[$this->getRequest()->getParam('txn_id')] = Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH;


        $customer = Mage::getModel('customer/customer');
        $customer
            ->setFirstname($kiosk['carrier_name_first'])
            ->setLastname($kiosk['carrier_name_last'])
        ;

        $billingAddress = Mage::getModel('sales/order_address');
        $billingAddress
            ->setStoreId(Mage::app()->getStore()->getId())
            ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING)
//            ->setCustomerId($customer->getId())
            ->setFirstname($kiosk['carrier_name_first'])
            ->setLastname($kiosk['carrier_name_last'])
            ->setStreet($kiosk['consignee_address_line'])
            ->setCity($kiosk['consignee_address_city'])
            ->setCountryId($kiosk['consignee_address_country'])
            ->setPostcode($kiosk['consignee_address_postcode']);

        $order_id = Iwings_Pochta_PaymentsController::createOrder($parcels, $customer, $billingAddress, $payment_transactions);


        $this->loadLayout();
        $this->renderLayout();
    }

    public function paymentDonePostAction() {

        $kiosk = Mage::getSingleton('core/session')->getKiosk();

        $declaration_required = $kiosk['declaration_required'];

        if ($declaration_required) {
            $this->_redirect('pochta/kiosk/transport');
        } else {
            return $this->processPostAction();
        }

    }

    public function transportAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function transportPostAction() {

        $kiosk = Mage::getSingleton('core/session')->getKiosk();
        $kiosk['transport_means_id'] = $this->getRequest()->getParam('transport_means_id');
        $kiosk['transport_nationality_code'] = $this->getRequest()->getParam('transport_nationality_code');
        $kiosk['transport_mode_code'] = 3;
        Mage::getSingleton('core/session')->setKiosk($kiosk);

        $this->_redirect('pochta/kiosk/borderline');
    }

    public function borderlineAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function borderlinePostAction() {

        $kiosk = Mage::getSingleton('core/session')->getKiosk();
        $kiosk['borderline'] = $this->getRequest()->getParam('borderline');
        $kiosk['carrier_date_in'] = $this->getRequest()->getParam('carrier_date_in');
        Mage::getSingleton('core/session')->setKiosk($kiosk);

        return $this->processPostAction();
    }

    public function processPostAction()
    {
        $this->initAttributeSets();

        $kiosk = Mage::getSingleton('core/session')->getKiosk();
        $declaration_required = $kiosk['declaration_required'];

        if (!$declaration_required) {
            $kiosk['borderline'] = 'External';
        }

        $this->prepareMovement($kiosk);

        $this->_redirect('pochta/kiosk/complete');
    }

    public function completeAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

}