<?php
class Iwings_Pochta_BaseController extends Mage_Core_Controller_Front_Action
{
    protected $frontendSiteId;
    protected $backendSiteId;

    protected $movementSetId;
    protected $documentTypeSetId;
    protected $parcelSetId;
    protected $simpleSetId;
    protected $partySetId;
    protected $serviceFeeSetId;

    protected function initAttributeSets() {

        /** @var $websites Mage_Core_Model_Resource_Website_Collection */
        $websites = Mage::getResourceModel('core/website_collection');

        foreach ($websites as $ws) {
            /** @var $ws Mage_Core_Model_Website */
            $siteCode = $ws->getCode();

            if ($siteCode == 'frontoffice') {
                $this->frontendSiteId = $ws->getId();
            }
            if ($siteCode == 'backoffice') {
                $this->backendSiteId = $ws->getId();
            }
        }


        $entityTypeId = Mage::getModel('eav/entity')->setType(Mage_Catalog_Model_Product::ENTITY)->getTypeId();
        /** @var $attrSets Mage_Eav_Model_Resource_Entity_Attribute_Set_Collection */
        $attrSets = Mage::getResourceModel('eav/entity_attribute_set_collection')->setEntityTypeFilter($entityTypeId);

        foreach ($attrSets as $set) {
            /** @var $set Mage_Eav_Model_Entity_Attribute_Set */
            $setName = $set->getAttributeSetName();

            if ($setName == 'Movement') {
                $this->movementSetId = $set->getId();
            }
            if ($setName == 'DocumentType') {
                $this->documentTypeSetId = $set->getId();
            }
            if ($setName == 'Parcel') {
                $this->parcelSetId = $set->getId();
            }
            if ($setName == 'SimpleProduct') {
                $this->simpleSetId = $set->getId();
            }
            if ($setName == 'PartyType') {
                $this->partySetId = $set->getId();
            }
            if ($setName == 'Fee') {
                $this->serviceFeeSetId = $set->getId();
            }
        }
    }


    protected function saveParcelAction($product = null)
    {
        $this->initAttributeSets();

        /** @var $product Mage_Catalog_Model_Product */
        if ($product == null) {
            $productId = $_POST['product_id'];
            if ($productId == null) {
                Mage::log("saveParcelAction: no product_id", Zend_log::EMERG);
            }

            $product = Mage::getModel('catalog/product')->load($productId);
        }

        $oldSuiteNum = $product->getBoxSuiteNum();
        $canChangeName = $product->getBoxOriginalTracking() == $product->getName();

        try{

            $product
                ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)
// immutable values
//                ->setWebsiteIds(array($this->backendSiteId, $this->frontendSiteId)) //website ID the product is assigned to, as an array
//                ->setAttributeSetId($this->parcelSetId) //ID of a attribute set named 'Parcel'
//                ->setTypeId('bundle') //product type
//                ->setCreatedAt(strtotime('now')) //product creation time
//                ->setStatus(1) //product status (1 - enabled, 2 - disabled)

                ->setSku($_POST['box_shelf'] . (1000000 + $product->getId()))               //SKU == Box Number
                ->setBoxSuiteNum($_POST['box_suite_num'])
                ->setBoxTrackingNum($_POST['box_tracking_num'])
                ->setBoxOriginalTracking($_POST['box_original_tracking'])
                ->setBoxShelf($_POST['box_shelf'])
                ->setClientVisibility($_POST['client_visibility'])

                ->setBoxLength($_POST['box_length'])
                ->setBoxWidth($_POST['box_width'])
                ->setBoxHeight($_POST['box_height'])
                ->setWeight($_POST['weight'])

                ->setConsigneeName($_POST['consignee_name'])

                ->setConsignorName($_POST['consignor_name'])
                ->setConsignorAddressLine($_POST['consignor_line'])
                ->setConsignorAddressCity($_POST['consignor_city'])
                ->setConsignorAddressCountry($_POST['consignor_country'])
                ->setConsignorAddressPostcode($_POST['consignor_postcode'])

                ->setStatePhysical($_POST['state_physical'])
//                ->setStateLogistic($_POST['state_logistic'])
                ->setStateCustom($_POST['state_custom'])
                ->setStatePayment($_POST['state_payment'])
                ->setStateControl($_POST['state_control'])
;
            if ($canChangeName) {
                $product
                    ->setName($_POST['box_original_tracking']);
            }


            if (isset($_POST['box_origin'])) {
                $product
                    ->setBoxOrigin($_POST['box_origin']);
            }
            if (isset($_POST['comment'])) {
                $product
                    ->setComment($_POST['comment']);
            }
            if (isset($_POST['box_store'])) {
                $product
                    ->setBoxStore($_POST['box_store']);
            }

//                ->setCategoryIds(array($categoryId)) //assign product to categories

//                ->setStockData(array(
//                        'use_config_manage_stock' => 0, //'Use config settings' checkbox
//                        'manage_stock'=>1, //manage stock
//                        'min_sale_qty'=>1, //Minimum Qty Allowed in Shopping Cart
//                        'max_sale_qty'=>1, //Maximum Qty Allowed in Shopping Cart
//                        'is_in_stock' => 1, //Stock Availability
//                        'qty' => 1 //qty
//                    )
//                )

;

            $suggestedFee = $this->prepareBundleContent($product);

            $relation_data = array();   //Mage::helper('pochta')->getServiceFeeData($product);
            $filledCount = 0;

            for ($i = 0; $i < count($_POST['sfee_name']); $i++) {
                if ($_POST['sfee_name'][$i] == "") continue;

                /** @var $serviceFee Mage_Catalog_Model_Product */
                if ($_POST['sfee_id'][$i] == "") {
                    $serviceFee = Mage::getModel('catalog/product');

                    $serviceFee
                        ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID) //you can set data in store scope
                        ->setWebsiteIds(array($this->backendSiteId, $this->frontendSiteId)) //website ID the product is assigned to, as an array
                        ->setAttributeSetId($this->serviceFeeSetId) //ID of a attribute set named 'Parcel'
                        ->setTypeId('virtual') //product type
                        ->setCreatedAt(strtotime('now')) //product creation time
                        ->setStatus(1) //product status (1 - enabled, 2 - disabled)

                        ->setName($_POST['sfee_name'][$i])
                        ->setSku(uniqid('sku'))
                        ->setUserPrice(round($_POST['sfee_user_price'][$i]))
                        ->setIsCancelable( $_POST['sfee_cancelable'] != null && array_search($i,$_POST['sfee_cancelable']) !== false )
                        ->setPrice(round($_POST['sfee_price'][$i-$filledCount]))
                        //                    ->setComment($_POST['comment'])
                    ;

                    if (empty($_POST['sfee_type'][$i])) {
                        $serviceFee
                            ->setFeeType(Mage::registry('fee_type_additional'));
                    } else {
                        $serviceFee
                            ->setFeeType($_POST['sfee_type'][$i]);
                    }

                    $serviceFee->save();
                } else {
                    $serviceFee = Mage::getModel('catalog/product')->load($_POST['sfee_id'][$i]);
                    $filledCount++;

                    $serviceFee
                        ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)
                        ->setName($_POST['sfee_name'][$i])
                        ->setUserPrice(round($_POST['sfee_user_price'][$i]))
                        ->setIsCancelable( $_POST['sfee_cancelable'] != null && array_search($i,$_POST['sfee_cancelable']) !== false )
                        //->setPrice(round($_POST['sfee_price'][$i]))  // do not change suggested price for already exists fees
                        //                    ->setComment($_POST['comment'])
                    ;

                    if ($serviceFee->getFeeType() == Mage::registry('fee_type_deposit')) {
                        $serviceFee
                            ->setPrice($suggestedFee)
                            ->setIsCancelable(true);
                    } elseif ($serviceFee->getFeeType() == Mage::registry('fee_type_base')) {
                        $serviceFee
                            ->setIsCancelable(false);
                    }

                    $serviceFee->save();
                }

                $relation_data[$serviceFee->getId()] = array('position' => 0);
            }

            $product->setUpSellLinkData($relation_data);


            $uploaddir = 'media/catalog/product/';
            for ($i = 0; $i < count($_FILES['parcel_image']['name']); $i++) {
                if ($_FILES['parcel_image']['name'][$i] == "") continue;
                $uploadfile = $uploaddir . basename($_FILES['parcel_image']['name'][$i]);
                move_uploaded_file($_FILES['parcel_image']['tmp_name'][$i], $uploadfile);
                $product
                    ->addImageToMediaGallery($uploadfile, null, true, false);
            }


            if ($oldSuiteNum != $product->getBoxSuiteNum()) {
                $productCategories = $product->getCategoryIds();

                $oldUser = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('suite')->addAttributeToFilter('suite', $oldSuiteNum)->load()->getFirstItem();
                $newUser = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('suite')->addAttributeToFilter('suite', $product->getBoxSuiteNum())->load()->getFirstItem();

                if ($oldUser != null) {
                    /** @var $oldCategory Mage_Catalog_Model_Category */
                    $oldCategory = Mage::getModel('catalog/category')->loadByAttribute('name', 'User_' . $oldUser->getId());

                    if ($oldCategory != null) {
                        $productCategories = array_diff($productCategories, array($oldCategory->getId()));
                    }
                }

                $newCategory = Mage::getModel('catalog/category')->loadByAttribute('name', 'User_' . $newUser->getId());
                if ($newCategory == null) {
                    /** @var $oldCategory Mage_Catalog_Model_Category */
                    $newCategory = Mage::getModel('catalog/category');

                    $newCategory
                        ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID) //you can set data in store scope
                        ->setName('User_' . $newUser->getId()) //category name
                        ->setDescription('Товары пользователя ' . $newUser->getName()); //category name

                    $newCategory->setIsActive(1);

                    $parentCategory = Mage::getModel('catalog/category')->load(2);      // 'Default Category' as parent
                    $newCategory->setPath($parentCategory->getPath());

                    $newCategory->save();
                }

                $productCategories[] = $newCategory->getId();

                $product->setCategoryIds($productCategories);
            }

            $isObjectNew = $product->isObjectNew();

            $product->save();

            if ($isObjectNew)
            {
                $product = Mage::getModel('catalog/product')->load($product->getId());
                $product
                    ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)
                    ->setName($_POST['box_original_tracking'])
                    ->setSku($_POST['box_shelf'] . (1000000 + $product->getId())); //SKU == Box Number
                $product->save();
            }


            return $product->getId();

        } catch(Exception $e){
            Mage::log($e->getMessage());
        }
    }

    public static function getBundledData($parcel) {

        /** @var $realProductType Mage_Bundle_Model_Product_Type */

        $realProductType = $parcel->getTypeInstance(true);
        $option_id =
            $realProductType
                ->getOptionsCollection($parcel)
                ->addFieldToFilter('title', Mage::registry('parcel_items_bundle_option_name'))
                ->getFirstItem()
                ->getOptionId();

        $bundleItems = $realProductType->getSelectionsCollection(array($option_id), $parcel)->addAttributeToSelect('weight');

        return $bundleItems;
    }

    protected function prepareBundleContent($product, $updateCustomInfo = true) {

        if ($product->isObjectNew()) {
            $option_id = 0;
        } else {
            $realProductType = $product->getTypeInstance(true);
            $option_id =
                $realProductType
                    ->getOptionsCollection($product)
                    ->addFieldToFilter('title', Mage::registry('parcel_items_bundle_option_name'))
                    ->getFirstItem()
                    ->getOptionId();
        }

        $bundleOptions = array(
            0 => array(
                'option_id' => $option_id != 0 ? $option_id : '',
                'title' => Mage::registry('parcel_items_bundle_option_name'),
                'type' => 'checkbox',
                'required' => 1
            ),
        );
        //setting the bundle options and selection data
        $product->setBundleOptionsData($bundleOptions);

        $suggestedFee = 0.0;

        $bundleSelections = array();
        for ($i = 0; $i < count($_POST['sproduct_name']); $i++) {
            if ($_POST['sproduct_name'][$i] == "") continue;

            /** @var $simpleProduct Mage_Catalog_Model_Product */
            if ($_POST['sproduct_id'][$i] == "") {
                $simpleProduct = Mage::getModel('catalog/product');

                $simpleProduct
                    ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID) //you can set data in store scope
                    ->setWebsiteIds(array($this->backendSiteId, $this->frontendSiteId)) //website ID the product is assigned to, as an array
                    ->setAttributeSetId($this->simpleSetId) //ID of a attribute set named 'Parcel'
                    ->setTypeId('simple') //product type
                    ->setCreatedAt(strtotime('now')) //product creation time
                    ->setStatus(1) //product status (1 - enabled, 2 - disabled)
                    ->setSku(uniqid('sku'))

                    ->setUserCurrency('EUR')
                ;
            } else {
                $simpleProduct = Mage::getModel('catalog/product')->load($_POST['sproduct_id'][$i]);

                $simpleProduct
                    ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)
                ;
                $simpleProduct->setDataChanges(false);   // hack for hasDataChanges()
            }

            if ($updateCustomInfo) {
                $simpleProduct
                    ->setWeight($_POST['sproduct_weight'][$i])

                    ->setTariffClassification($_POST['sproduct_tariff'][$i])
                    ->setGoodsDescription($_POST['sproduct_name'][$i])

                    ->setPackagingTypeCode($_POST['sproduct_packaging_type_code'][$i])
                    ->setPackagingMarksId($_POST['sproduct_packaging_marks_id'][$i])
                    ->setPackagingQuantity(round($_POST['sproduct_qty'][$i]))
//                    ->setComment($_POST['comment'])
                    ->setUserCurrency($_POST['sproduct_user_currency'][$i])
                ;

                $userCurrency = $_POST['sproduct_user_currency'][$i];

                $rate = Mage::helper('directory')->currencyConvert(1, 'EUR', $userCurrency);
                $userPrice = $_POST['sproduct_user_price'][$i];
                $eurPrice = $userCurrency == 'EUR' ? $userPrice : $userPrice / $rate;

            } else {
                $userCurrency = $simpleProduct->getUserCurrency();

                $rate = Mage::helper('directory')->currencyConvert(1, 'EUR', $userCurrency);
                $eurPrice = $_POST['sproduct_user_price'][$i];
                $userPrice = $userCurrency == 'EUR' ? $eurPrice : $eurPrice * $rate;
            }

            $simpleProduct
                ->setName($_POST['sproduct_name'][$i])
                ->setUserPrice($userPrice)
                ->setPrice($eurPrice)
            ;

            $suggestedFee += $eurPrice * ($_POST['sproduct_qty'][$i] == "" ? 1 : round($_POST['sproduct_qty'][$i]));

            if ($simpleProduct->isObjectNew() || $simpleProduct->hasDataChanges()) {    // useless as any assignment (regardless of old and new data values) leads to _hasDataChanges
                $simpleProduct->save();
            }

            $bundleSelections[0][] = array(
                'option_id' => $option_id !== 0 ? $option_id : '',
                'selection_id' => $_POST['sproduct_selection'][$i],
                'product_id' => $simpleProduct->getId(),
                'selection_price_value' => $simpleProduct->getPrice(),
                //'selection_price_type' => 0,
                'selection_qty' => $_POST['sproduct_qty'][$i] == "" ? 1 : round($_POST['sproduct_qty'][$i]),
                'selection_can_change_qty' => 0,
                'position' => 0,
                'is_default' => 1
            );

        }

        $product->setBundleSelectionsData($bundleSelections);

        $product->setCanSaveBundleSelections(true);
        $product->setAffectBundleProductSelections(true);

        //registering a product because of Mage_Bundle_Model_Selection::_beforeSave
        Mage::register('product', $product);

        return $suggestedFee * 0.3;

    }

    public function prepareMovement($movementInfo) {
        $takeoutTime = strtotime('now');

        try {

            /** @var $movement Mage_Catalog_Model_Product */
            $movement = Mage::getModel('catalog/product');
            $movement
                ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID) //you can set data in store scope
                ->setTypeId('virtual')                              //product type
                ->setSku(uniqid('sku'))
//                ->setName('Move from External to ' . $takeout->getResource()->getAttribute('reception_type')->getSource()->getOptionText($reception_type))
                ->setStatus(1)                                      //product status (1 - enabled, 2 - disabled)
//                ->setVisibility(?)
                ->setCreatedAt($takeoutTime)                    //product creation time

                ->setWebsiteIds(array($this->backendSiteId))        //website ID the product is assigned to, as an array
                ->setAttributeSetId($this->movementSetId)            //ID of a attribute set named 'Arrival'

                ->setReceptionType(Mage::registry('reception_type_na'))

                ->setCarrierNameFirst($movementInfo['carrier_name_first'])
                ->setCarrierNameLast($movementInfo['carrier_name_last'])
                ->setConsigneeAddressLine($movementInfo['consignee_address_line'])
                ->setConsigneeAddressCity($movementInfo['consignee_address_city'])
                ->setConsigneeAddressCountry($movementInfo['consignee_address_country'])
                ->setConsigneeAddressPostcode($movementInfo['consignee_address_postcode'])

                ->setTransportModeCode($movementInfo['transport_mode_code'])
                ->setTransportNationalityCode($movementInfo['transport_nationality_code'])
                ->setTransportMeansId($movementInfo['transport_means_id'])
                ->setConveyanceReferenceId(null)

                ->setMvtSrcWhs(Mage::getResourceSingleton('catalog/product')->getAttribute('box_store')->getSource()->getOptionText(Mage::getSingleton('core/session')->getSelectedStoreId()))
                ->setMvtSrcStatus(Mage::registry('mvt_src_status_planned'))
//                ->setMvtSrcActualDate($takeoutTime)
                ->setMvtSrcPlannedDate($takeoutTime)

                ->setMvtDstWhs($movementInfo['borderline'])
                ->setMvtDstStatus(Mage::registry('mvt_dst_status_notyet'))
//                ->setMvtDstActualDate($movementInfo['carrier_date_in'])
                ->setMvtDstPlannedDate($movementInfo['carrier_date_in'])

                ->setComment($movementInfo['comment'])
            ;

            $movement
                ->setName('Move from ' . $movement->getMvtSrcWhs() . ' to ' . $movement->getMvtDstWhs());
            //$takeout->save();

//            $relation_data = array();
//            foreach ($movementInfo['parcels'] as $parcelId) {
//
//                /** @var $parcel Mage_Catalog_Model_Product */
//                $parcel = Mage::getModel('catalog/product')->load($parcelId);
//
//                /** @var $declarations Mage_Catalog_Model_Resource_Product_Link_Product_Collection */
//                $documents = $parcel->getRelatedProductCollection()->addAttributeToFilter('attribute_set_id', $this->documentTypeSetId)->setOrder('date', 'asc');
//                $document = $documents->getLastItem();
//
//                $relation_data[$document->getId()] = array('position' => 0);
//            }
//            $movement->setRelatedLinkData($relation_data);

            $movement->save();

            /* **********   update parcel ********* */
            foreach ($movementInfo['parcels'] as $parcelId) {

                /** @var $parcel Mage_Catalog_Model_Product */
                $parcel = Mage::getModel('catalog/product')->load($parcelId);

//                $parcel->setBoxStore(Mage::registry('box_store_na'));

                $relation_data = Mage::helper('pochta')->getMovementData($parcel);
                $relation_data[$movement->getId()] = array('position' => 0);
                $parcel->setCrossSellLinkData($relation_data);

                $parcel->save();

            }

            return $movement;

        } catch(Exception $e){
            Mage::log($e->getMessage());
        }
    }

}