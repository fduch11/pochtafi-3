<?php
class Iwings_Actionaudit_Helper_Observer
{
    public function logAudit(Varien_Event_Observer $observer)
  	{
        /** @var $event Varien_Event */
        $event = $observer->getEvent();

        $object = $event->getDataObject();

        if ($object instanceof Mage_Catalog_Model_Product && Mage::getStoreConfig("Iwings_Actionaudit/Actionaudit/Product")) {
            /** @var $product Mage_Catalog_Model_Product */
            $product = $event->getProduct();

            //$msg = $product->isDeleted() ? 'Deleted' : $product->isObjectNew() ? 'Added' : 'Updated';
            $action = strpos($event->getName(), '_delete_') !== false ? 'Deleted' : ($product->getOrigData() == null ? 'Added' : 'Updated');

            $msg = '';
            /** @var $attrCollection Mage_Catalog_Model_Resource_Product_Attribute_Collection */
            $attrCollection = Mage::getResourceModel('catalog/product_attribute_collection')->addFilter('is_user_defined', 1);
            foreach ($attrCollection as $attribute) {
                /** @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
//                if (in_array('bundle', $attribute->getApplyTo()))
                {
                    $attrCode = $attribute->getAttributeCode();
                    $attr_old = $product->getOrigData($attrCode);
                    $attr_new = $product->getData($attrCode);

                    if ($attr_old != $attr_new) {
                        $msg .= $attribute->getFrontendLabel() . ': ' . ($attr_old == null ? '(empty)' : $attr_old) . ' => ' . ($attr_new == null ? '(empty)' : $attr_new) . '<br/>';
                    } else if ($action == 'Deleted') {
                        $msg .= $attribute->getFrontendLabel() . ': ' . ($attr_old == null ? '(empty)' : $attr_old) . ' => ' . '<br/>';
                    }
                }
            }

            // save only changed products
            if ($msg != '') {
                if (Mage::getSingleton('admin/session')->isLoggedIn()) {
                    // we are logged in admin interface

                    /** @var $current_user Mage_Admin_Model_User */
                    $current_user = Mage::getSingleton('admin/session')->getUser();
                    $userName = $current_user->getUsername();
                    $roleId = implode('', $current_user->getRoles());
                    $roleName = Mage::getModel('admin/roles')->load($roleId)->getRoleName();

                    $storeName = Mage::app()->getWebsite()->getName() . '/' . Mage::app()->getStore()->getName();
                    if ($storeName == '/')
                        $storeName = 'default';
                } else if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                    // we are logged as a regular customer

                    /** @var $current_user Mage_Customer_Model_Customer */
                    $current_user = Mage::getSingleton('customer/session')->getCustomer();
                    $userName = $current_user->getName();
                    if (trim($userName) == '') {
                        $userName = $current_user->getEmail();
                    }
                    $roleId = $current_user->getGroupId();
                    $roleName = Mage::getSingleton('customer/group')->load($roleId)->getCustomerGroupCode();

                    $storeName = Mage::app()->getWebsite()->getName() . '/' . Mage::app()->getStore()->getName();
                    if ($storeName == '/')
                        $storeName = 'default';
                } else {
//                throw new Mage_Core_Exception(
//                    $this->__('You should be either logged as Admin or as Customer.')
//                );

                    $userName = 'not logged';
                    $roleName = 'not logged';
                    $storeName = Mage::app()->getStore()->getName();
                    if ($storeName == '')
                        $storeName = 'default';
                }


                $actionaudit_model = Mage::getModel('actionaudit/actionaudit');
                $data['log_action'] = $action;
                $data['adminrole'] = $roleName;
                $data['website'] = $storeName;
                $data['updated_by'] = $userName;
                $data['log_entity'] = $product->getName();
                //$data['log_entity'] = $product->getName() . ($product->getName() != $product->getOrigData('name') ? ' ('.$product->getOrigData('name').')' : '');
                $data['json_old'] = json_encode($product->getOrigData());
                $data['json_new'] = json_encode($product->getData());
                $actionaudit_model->setData($data);
                $actionaudit_model->save();
            }
        }

        return $this;
  	}
	
	
}

?>