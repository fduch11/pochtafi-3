<?php

class Mage_Adminhtml_Block_Customer_Renderer_Notifyme extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $notify_me = explode(',',$row->getNotifyMe());
        $html = "";
        if(count($notify_me)>0)
        {
            $html ="<ul>";
            foreach($notify_me as $value)
            {
                $_customerModel = Mage::getModel('customer/customer');
                $attribute = $_customerModel->getResource()->getAttribute("notify_me");
                if ($attribute ->usesSource()) {
                    $html .= "<li>".$attribute ->getSource()->getOptionText("$value")."</li>";
                }
            }
            $html .= "</ul>";
        }
        return $html ;
    }
}
