<?php

class Hybridauth_HybridAuth_Helper_Data extends Mage_Core_Helper_Abstract{ 
	public function login($provider){
		$hybridProvider = null;
		try {
			require_once( Mage::getBaseDir('lib') . "/hybridauth/Hybrid/StorageMagento.php" );
			require_once(Mage::getBaseDir('lib') . '/hybridauth/Hybrid/Auth.php');
			
			$hybridauth = new Hybrid_Auth( Mage::getBaseDir('app') . '/etc/config_hybridauth.php' );
			$hybridProvider = $hybridauth->authenticate( $provider );
			
			$user_profile = (array)$hybridProvider->getUserProfile();
			if($hybridProvider->isUserConnected() && $provider && $user_profile['identifier']){
				$collection = Mage::getModel('customer/customer')->getCollection();
				$object = $collection
						->addAttributeToFilter('social_net',array('eq' => $provider))
						->addAttributeToFilter('social_identifier', array('eq' => $user_profile['identifier']))
						->getFirstItem();
				if(!$object->getId()){
					Mage::getSingleton("customer/session")->setUserProfile(serialize($user_profile));
					Mage::getSingleton("customer/session")->setProvider($provider);
					return "auth/index/email";
				}
				if($object->getConfirmation()){
					return 'customer/account/emailconfirmsent';
				}
				//$session->setCustomerAsLoggedIn($object);
                //$successUrl = $this->_welcomeCustomer($object, true);
				Mage::getSingleton ('customer/session')->loginById ($object->getId());
				return 'customer/account/index';
			}
		}
		catch( Exception $e ){
			//TODO add error into log
		}
		if($hybridProvider)
		   $hybridProvider->logout();
		return false;
	}
	public function hauth(){
		require_once( Mage::getBaseDir('lib') . "/hybridauth/Hybrid/StorageMagento.php" );
		require_once( Mage::getBaseDir('lib') . "/hybridauth/Hybrid/Auth.php" );
		require_once( Mage::getBaseDir('lib') . "/hybridauth/Hybrid/Endpoint.php" ); 

		Hybrid_Endpoint::process();
	}
	private function createCustomer($user_profile, $provider, $email){

        $suite = rand(100000, 999999);
        /* test suite for uniqueness */
        while (Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('suite')->addAttributeToFilter('suite', $suite)->getSize() != 0) {
            $suite = rand(100000, 999999);
        }

		$websiteId = Mage::app()->getWebsite()->getId();
		$store = Mage::app()->getStore();
 		$customer = Mage::getModel("customer/customer");
		$customer->setWebsiteId($websiteId)
            ->setStore($store)
            ->setFirstname($user_profile['firstName'])
            ->setLastname($user_profile['lastName'])
            ->setEmail($email)
            ->setPassword($customer->generatePassword (8))
			->setSocial_net($provider)
			->setSocial_identifier($user_profile['identifier'])
            ->setSuite($suite)
			->save();
		$customer->setConfirmation(md5($customer->generatePassword (8)));
		$customer->save();
		$customer->sendNewAccountEmail('confirmation');
		return $customer;
	}
	public function emailActivation($email){
		$user_profile = unserialize(Mage::getSingleton("customer/session")->getUserProfile());
		$provider = Mage::getSingleton("customer/session")->getProvider();
		$this->createCustomer($user_profile, $provider, $email);
	}
} 