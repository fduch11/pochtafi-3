<?php
Mage::register('is_production_environment', isset($_SERVER['POCHTA_PRODUCTION']));

Mage::register('box_origin_european', 81);
Mage::register('box_origin_overseas', 80);

Mage::register('reception_type_portland', 119);
Mage::register('reception_type_vantaa', 118);
Mage::register('reception_type_lappeenranta', 117);
Mage::register('reception_type_imatra', 116);
Mage::register('reception_type_na', 126);
Mage::register('reception_type_eu_lappeenranta', 125);
Mage::register('reception_type_eu_imatra', 124);

Mage::register('warehouse_type_eu', 115);
Mage::register('warehouse_type_tt', 113);
Mage::register('warehouse_type_vv', 114);

Mage::register('box_store_portland', 108);
Mage::register('box_store_imatra', 109);
Mage::register('box_store_lappeenranta', 110);
Mage::register('box_store_default', 110);
Mage::register('box_store_na', 111);
Mage::register('box_store_vantaa', 112);

Mage::register('notify_me_email', 77);
Mage::register('notify_me_phone', 78);

Mage::register('state_payment_not_ready', 102);
Mage::register('state_payment_ready_for_payment', 101);
Mage::register('state_payment_paid', 99);
Mage::register('state_payment_delayed', 103);
Mage::register('state_payment_deposit_returned', 98);
Mage::register('state_payment_pay_cash', 132);

Mage::register('mvt_src_status_left', 122);
Mage::register('mvt_src_status_planned', 120);
Mage::register('mvt_src_status_assembled', 133);

Mage::register('mvt_dst_status_complete', 121);
Mage::register('mvt_dst_status_notyet', 123);

Mage::register('state_custom_na', 134);
Mage::register('state_custom_client_info_required', 97);
Mage::register('state_custom_operator_info_required', 131);
Mage::register('state_custom_ready_for_t1', 96);
Mage::register('state_custom_t1_accepted', 94);
Mage::register('state_custom_t1_rejected', 93);
Mage::register('state_custom_t1_released', 92);
Mage::register('state_custom_t1_sent', 95);

Mage::register('website_frontoffice', 2);
Mage::register('website_backoffice', 1);

Mage::register('state_physical_good', 82);
Mage::register('state_physical_broken', 83);

Mage::register('state_control_blocked_by_custom', 103);
Mage::register('state_control_normal', 104);

Mage::register('attr_set_id_movement', 24);
Mage::register('attr_set_id_document', 22);
Mage::register('attr_set_id_parcel', 25);
Mage::register('attr_set_id_simple', 26);
Mage::register('attr_set_id_partytype', 29);
Mage::register('attr_set_id_fitdec', 31);
Mage::register('attr_set_id_fitacc', 33);
Mage::register('attr_set_id_fitref', 35);
Mage::register('attr_set_id_fitrel', 34);
Mage::register('attr_set_id_fee', 36);
Mage::register('attr_set_id_fitgdr', 37);
Mage::register('attr_set_id_declaration', 38);

Mage::register('customer_default_place_lapa', 11);
Mage::register('customer_default_place_imatra', 12);

Mage::register('parcel_items_bundle_option_name', 'parcel items');     // not related to DB, just global constant

Mage::register('fee_type_base', 137);
Mage::register('fee_type_deposit', 136);
Mage::register('fee_type_additional', 135);
?>