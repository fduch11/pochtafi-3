<?php

set_include_path(get_include_path().PATH_SEPARATOR.__DIR__. '/model/dme');
set_include_path(get_include_path().PATH_SEPARATOR.__DIR__. '/model/ncts');
spl_autoload_extensions('.php');
spl_autoload_register();

require 'vendor/autoload.php';


class CustomsNotifyCallback {

    function Notify($args) {

        $folderName = Mage::getBaseDir('log');
        $fullPath = $folderName .DS. 'customs_notify.txt';
        file_put_contents($fullPath, json_encode($args) . PHP_EOL . PHP_EOL, FILE_APPEND);

        /** @var $notifyRequestHeader NotifyRequestHeader */
        $notifyRequestHeader = $args->RequestHeader;

        /** @var $messageInformation MessageInformation */
        $messageInformation = $args->MessageInformation;

        $notifyResponseHeader = new NotifyResponseHeader($notifyRequestHeader->IntermediaryBusinessId, date('c'));

        $result = new NotifyResponse();
        $result->ResponseHeader = $notifyResponseHeader;
        $result->ResponseCode = '000';
        $result->ResponseText = 'OK';

        return $result;
    }
}