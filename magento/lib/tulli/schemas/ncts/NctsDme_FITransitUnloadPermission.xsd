<?xml version="1.0" encoding="UTF-8"?>
<!-- NCTS Direct Message Exchange schema of FITransitUnloadPermission message -->
<xs:schema xmlns:ncts="http://tulli.fi/schema/external/ncts/dme/v1" xmlns:udt="http://tulli.fi/schema/external/common/dme/v1_0/udt" xmlns:cdt="http://tulli.fi/schema/external/common/dme/v1_0/cdt" xmlns:qdt="http://tulli.fi/schema/external/common/dme/v1_0/qdt" xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="http://tulli.fi/schema/external/ncts/dme/v1" elementFormDefault="qualified" version="v1_1">
	<!-- Common schema components -->
	<xs:include schemaLocation="NctsDme_QualifiedType.xsd"/>
	<xs:import namespace="http://tulli.fi/schema/external/common/dme/v1_0/qdt" schemaLocation="Dme_QualifiedType.xsd"/>
	<xs:import namespace="http://tulli.fi/schema/external/common/dme/v1_0/cdt" schemaLocation="Dme_CodeListType.xsd"/>
	<xs:import namespace="http://tulli.fi/schema/external/common/dme/v1_0/udt" schemaLocation="Dme_UnqualifiedType.xsd"/>
	<!-- Root Element -->
	<xs:element name="FITransitUnloadPermission">
		<xs:annotation>
			<xs:documentation xml:lang="en">FITULD - Unloading permission message (IE43).</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:complexContent>
				<xs:extension base="ncts:FITransitUnloadPermissionType">
					<xs:attribute name="schemaVersion" type="ncts:CompatibleSchemaVersionType" use="required">
						<xs:annotation>
							<xs:documentation xml:lang="en">List of compatible schema versions, containing this version and all accepted earlier compatible versions.</xs:documentation>
						</xs:annotation>
					</xs:attribute>
				</xs:extension>
			</xs:complexContent>
		</xs:complexType>
		<xs:unique name="uniquePermissionGoodsItemSequenceNumber">
			<xs:annotation>
				<xs:documentation xml:lang="en">GoodsItem/ItemSequenceNumber is unique within message.</xs:documentation>
			</xs:annotation>
			<!-- Unique name is required for global definitions -->
			<xs:selector xpath="ncts:GoodsItem/ncts:ItemSequenceNumber"/>
			<xs:field xpath="."/>
		</xs:unique>
	</xs:element>
	<xs:complexType name="FITransitUnloadPermissionType">
		<xs:sequence>
			<xs:element name="MessageInterchange" type="ncts:MessageInterchangeType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Message interchange information.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Permission" type="ncts:PermissionType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Unloading persmission.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="GoodsItem" type="ncts:PermissionGoodsItemType" maxOccurs="999">
				<xs:annotation>
					<xs:documentation xml:lang="en">Goods item information.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<!--Acceptance-->
	<xs:complexType name="PermissionType">
		<xs:sequence>
			<xs:element name="MovementReferenceID" type="qdt:MovementReferenceIDType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Movement reference number (MRN), issued by Customs.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="TransitTypeCode" type="cdt:TransitTypeCodeType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Transit declaration type code, FI Customs code list 0081.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="AcceptanceDate" type="udt:DateType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Acceptance date of declaration.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="PermissionDate" type="udt:DateType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Permission date of unloading permission.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="DispatchCountryCode" type="cdt:CountryCodeType" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Country of dispatch.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="DestinationCountryCode" type="cdt:CountryCodeType" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Destination country.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="TransitDepartureOffice" type="ncts:CustomsOfficeContactType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Customs office where transit procedure starts.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="TransitPresentationOffice">
				<xs:annotation>
					<xs:documentation xml:lang="en">Customs office where goods are to be presented for release from transit procedure.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:complexContent>
						<xs:extension base="ncts:CustomsOfficeContactType">
							<xs:sequence>
								<xs:element name="Contact" type="ncts:ContactInformatioType" minOccurs="0">
									<xs:annotation>
										<xs:documentation xml:lang="en">Customs office contact details.</xs:documentation>
									</xs:annotation>
								</xs:element>
							</xs:sequence>
						</xs:extension>
					</xs:complexContent>
				</xs:complexType>
			</xs:element>
			<xs:element name="DepartureTransportMeans" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Information on the means of transport on which the goods are directly loaded at the time of transit formalities.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:complexContent>
						<xs:restriction base="ncts:TransportMeansType">
							<xs:sequence>
								<xs:element name="TransportMeansNationalityCode" type="cdt:CountryCodeType" minOccurs="0"/>
								<xs:element name="TransportMeansID" type="ncts:TransportMeansIDType" minOccurs="0"/>
							</xs:sequence>
						</xs:restriction>
					</xs:complexContent>
				</xs:complexType>
			</xs:element>
			<xs:element name="ContainerTransportIndicator" type="udt:IndicatorType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Indicates, wether the goods are or are presumed to be transported in container(s) when crossing the Community border.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Sealing" type="ncts:SealingType" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Seals information.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="GoodsItemQuantity" type="ncts:GoodsItemQuantityType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Goods item quantity.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="TotalPackageQuantity" type="ncts:TotalPackageQuantityType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Total package quantity.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="TotalGrossMassMeasure" type="ncts:WeightMeasureType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Total gross mass for all goods items in transit declaration.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Consignor" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Consignor party.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:complexContent>
						<xs:restriction base="ncts:PartyType">
							<xs:sequence>
								<xs:element name="ID" type="ncts:TraderIDType" minOccurs="0"/>
								<xs:element name="Name" type="qdt:NameType" minOccurs="0"/>
								<xs:element name="Address" type="ncts:AddressType" minOccurs="0"/>
							</xs:sequence>
						</xs:restriction>
					</xs:complexContent>
				</xs:complexType>
			</xs:element>
			<xs:element name="Consignee" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Consignee party.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:complexContent>
						<xs:restriction base="ncts:PartyType">
							<xs:sequence>
								<xs:element name="ID" type="ncts:TraderIDType" minOccurs="0"/>
								<xs:element name="Name" type="qdt:NameType" minOccurs="0"/>
								<xs:element name="Address" type="ncts:AddressType" minOccurs="0"/>
							</xs:sequence>
						</xs:restriction>
					</xs:complexContent>
				</xs:complexType>
			</xs:element>
			<xs:element name="AuthorisedConsignee" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Authorised consignee party.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:complexContent>
						<xs:restriction base="ncts:PartyType">
							<xs:sequence>
								<xs:element name="ID" type="ncts:TraderIDType"/>
								<xs:element name="IDExtension" type="qdt:IDExtensionType" minOccurs="0"/>
							</xs:sequence>
						</xs:restriction>
					</xs:complexContent>
				</xs:complexType>
			</xs:element>
			<xs:element name="Principal">
				<xs:annotation>
					<xs:documentation xml:lang="en">Principal trader party.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:complexContent>
						<xs:restriction base="ncts:PartyType">
							<xs:sequence>
								<xs:element name="ID" type="ncts:TraderIDType" minOccurs="0"/>
								<xs:element name="Name" type="qdt:NameType"/>
								<xs:element name="Address">
									<xs:complexType>
										<xs:complexContent>
											<xs:restriction base="ncts:AddressType">
												<xs:sequence>
													<xs:element name="Line" type="qdt:NameType"/>
													<xs:element name="PostcodeID" type="cdt:PostcodeIDType"/>
													<xs:element name="CityName" type="qdt:NameType"/>
													<xs:element name="CountryCode" type="cdt:CountryCodeType"/>
												</xs:sequence>
											</xs:restriction>
										</xs:complexContent>
									</xs:complexType>
								</xs:element>
							</xs:sequence>
						</xs:restriction>
					</xs:complexContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="PermissionGoodsItemType">
		<xs:sequence>
			<xs:element name="ItemSequenceNumber" type="qdt:ItemSequenceNumberType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Ordinal number indicating the position in a sequence, anf identifying the item within the sequence.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="TransitTypeCode" type="cdt:TransitTypeCodeType" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Transit declaration type code, FI Customs code list 0081.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="AdditionalDocument" minOccurs="0" maxOccurs="99">
				<xs:annotation>
					<xs:documentation xml:lang="en">Additional document information.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:complexContent>
						<xs:restriction base="ncts:DocumentType">
							<xs:sequence>
								<xs:element name="DocumentTypeCode" type="cdt:DocumentTypeCodeType" minOccurs="0"/>
								<xs:element name="DocumentID" type="ncts:DocumentIDType" minOccurs="0"/>
								<xs:element name="SupplementaryInformation" type="qdt:SupplementaryInformationType" minOccurs="0"/>
							</xs:sequence>
						</xs:restriction>
					</xs:complexContent>
				</xs:complexType>
			</xs:element>
			<xs:element name="AdditionalInformation" minOccurs="0" maxOccurs="99">
				<xs:annotation>
					<xs:documentation xml:lang="en">Additional information.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:complexContent>
						<xs:restriction base="ncts:AdditionalInformationType">
							<xs:sequence>
								<xs:element name="SpecialMentionCode" type="cdt:SpecialMentionCodeType" minOccurs="0"/>
								<xs:element name="EUExportIndicator" type="udt:IndicatorType" minOccurs="0"/>
								<xs:element name="ExportingCountryCode" type="cdt:CountryCodeType" minOccurs="0"/>
							</xs:sequence>
						</xs:restriction>
					</xs:complexContent>
				</xs:complexType>
			</xs:element>
			<xs:element name="DispatchCountryCode" type="cdt:CountryCodeType" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Country of dispatch.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="DestinationCountryCode" type="cdt:CountryCodeType" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Destination country.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="TransportEquipment" type="ncts:TransportEquipmentType" minOccurs="0" maxOccurs="99">
				<xs:annotation>
					<xs:documentation xml:lang="en">Transport equipment information.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Packaging" type="ncts:PackagingType" maxOccurs="99">
				<xs:annotation>
					<xs:documentation xml:lang="en">Packaging information.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Commodity">
				<xs:annotation>
					<xs:documentation xml:lang="en">Goods commodity and classification.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:complexContent>
						<xs:restriction base="ncts:CommodityType">
							<xs:sequence>
								<xs:element name="TariffClassification" type="ncts:TariffClassificationType" minOccurs="0"/>
								<xs:element name="GoodsDescription" type="ncts:GoodsDescriptionType"/>
							</xs:sequence>
						</xs:restriction>
					</xs:complexContent>
				</xs:complexType>
			</xs:element>
			<xs:element name="SensitiveGoods" type="ncts:SensitiveGoodsType" minOccurs="0" maxOccurs="9">
				<xs:annotation>
					<xs:documentation xml:lang="en">Goods sensitivity information.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="GrossMassMeasure" type="ncts:WeightMeasureType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Total weight (mass) of goods.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="NetWeightMeasure" type="ncts:WeightMeasureType" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Weight (mass) of the goods without packing material.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Consignor" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Consignor party.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:complexContent>
						<xs:restriction base="ncts:PartyType">
							<xs:sequence>
								<xs:element name="ID" type="ncts:TraderIDType" minOccurs="0"/>
								<xs:element name="Name" type="qdt:NameType" minOccurs="0"/>
								<xs:element name="Address" type="ncts:AddressType" minOccurs="0"/>
							</xs:sequence>
						</xs:restriction>
					</xs:complexContent>
				</xs:complexType>
			</xs:element>
			<xs:element name="Consignee" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Consignee party.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:complexContent>
						<xs:restriction base="ncts:PartyType">
							<xs:sequence>
								<xs:element name="ID" type="ncts:TraderIDType" minOccurs="0"/>
								<xs:element name="Name" type="qdt:NameType" minOccurs="0"/>
								<xs:element name="Address" type="ncts:AddressType" minOccurs="0"/>
							</xs:sequence>
						</xs:restriction>
					</xs:complexContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
</xs:schema>
