<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName MessageInformation
 * @var MessageInformation
 */
class MessageInformation
	{



	/**                                                                       
		@param fi\tulli\ws\corporateservicetypes\v1\MessageStatus $MessageStatus [optional] Not present in UploadResponse.
		@param  $MessageStoredTimestamp [optional] Time and date when the message was stored.
		@param  $MessageDownloadedTimestamp [optional] Time and date when the message was downloaded (if download operation is done).
		@param fi\tulli\schema\corporateservice\v1\BusinessId $DeclarantBusinessId [optional] The business identity code of the party who is declarant.
	*/                                                                        
	public function __construct($MessageStorageId = null, $MessageStatus = null, $Application = null, $ControlReference = null, $MessageStoredTimestamp = null, $MessageDownloadedTimestamp = null, $DeclarantBusinessId = null, $ContentFormat = null)
	{
		$this->MessageStorageId = $MessageStorageId;
		$this->MessageStatus = $MessageStatus;
		$this->Application = $Application;
		$this->ControlReference = $ControlReference;
		$this->MessageStoredTimestamp = $MessageStoredTimestamp;
		$this->MessageDownloadedTimestamp = $MessageDownloadedTimestamp;
		$this->DeclarantBusinessId = $DeclarantBusinessId;
		$this->ContentFormat = $ContentFormat;
	}
	
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName MessageStorageId
	 * @var fi\tulli\schema\corporateservice\v1\MessageStorageId
	 */
	public $MessageStorageId;
	/**
	 * @Definition Not present in UploadResponse.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlMinOccurs 0
	 * @xmlName MessageStatus
	 * @var fi\tulli\ws\corporateservicetypes\v1\MessageStatus
	 */
	public $MessageStatus;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName Application
	 * @var fi\tulli\schema\corporateservice\v1\Application
	 */
	public $Application;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlMinOccurs 0
	 * @xmlName ControlReference
	 * @var fi\tulli\schema\corporateservice\v1\Reference
	 */
	public $ControlReference;
	/**
	 * @Definition Time and date when the message was stored.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName MessageStoredTimestamp
	 */
	public $MessageStoredTimestamp;
	/**
	 * @Definition Time and date when the message was downloaded (if download operation is done).
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlMinOccurs 0
	 * @xmlName MessageDownloadedTimestamp
	 */
	public $MessageDownloadedTimestamp;
	/**
	 * @Definition The business identity code of the party who is declarant.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName DeclarantBusinessId
	 * @var fi\tulli\schema\corporateservice\v1\BusinessId
	 */
	public $DeclarantBusinessId;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlMinOccurs 0
	 * @xmlName ContentFormat
	 * @var fi\tulli\schema\corporateservice\v1\ContentFormat
	 */
	public $ContentFormat;


} // end class MessageInformation
