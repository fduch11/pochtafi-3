<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName DownloadListRequest
 * @var DownloadListRequest
 */
class DownloadListRequest
	{



	/**                                                                       
	*/                                                                        
	public function __construct($RequestHeader = null, $DownloadMessageListFilteringCriteria = null)
	{
		$this->RequestHeader = $RequestHeader;
		$this->DownloadMessageListFilteringCriteria = $DownloadMessageListFilteringCriteria;
	}
	
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName RequestHeader
	 * @var fi\tulli\ws\corporateservicetypes\v1\RequestHeader
	 */
	public $RequestHeader;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName DownloadMessageListFilteringCriteria
	 * @var fi\tulli\ws\corporateservicetypes\v1\DownloadMessageListFilteringCriteria
	 */
	public $DownloadMessageListFilteringCriteria;


} // end class DownloadListRequest
