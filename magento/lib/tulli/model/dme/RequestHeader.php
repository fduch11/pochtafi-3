<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName RequestHeader
 * @var RequestHeader
 * @xmlDefinition This element contains request information related to the transmitted data. This header is built by the intermediary.
 */
class RequestHeader
	{



	/**                                                                       
		@param fi\tulli\schema\corporateservice\v1\BusinessId $IntermediaryBusinessId [optional] The business identity code of the message intermediary (web service requester) of this request message. The intermediary can be a third party organization.
		@param  $Timestamp [optional] Date and time when the request was sent. Data type is ISODateTime. If no timezone is specified, the Finnish timezone is assumed.
		@param  $Language [optional] Code indicating the language used in the informative data elements. At the moment, only the value EN (English) is supported.
		@param  $IntermediarySoftwareInfo [optional] The name and version of the software which was used to send this request. Informal format
	*/                                                                        
	public function __construct($IntermediaryBusinessId = null, $Timestamp = null, $Language = null, $IntermediarySoftwareInfo = null)
	{
		$this->IntermediaryBusinessId = $IntermediaryBusinessId;
		$this->Timestamp = $Timestamp;
		$this->Language = $Language;
		$this->IntermediarySoftwareInfo = $IntermediarySoftwareInfo;
	}
	
	/**
	 * @Definition The business identity code of the message intermediary (web service requester) of this request message. The intermediary can be a third party organization.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName IntermediaryBusinessId
	 * @var fi\tulli\schema\corporateservice\v1\BusinessId
	 */
	public $IntermediaryBusinessId;
	/**
	 * @Definition Date and time when the request was sent. Data type is ISODateTime. If no timezone is specified, the Finnish timezone is assumed.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName Timestamp
	 */
	public $Timestamp;
	/**
	 * @Definition Code indicating the language used in the informative data elements. At the moment, only the value EN (English) is supported.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName Language
	 */
	public $Language;
	/**
	 * @Definition The name and version of the software which was used to send this request. Informal format
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName IntermediarySoftwareInfo
	 */
	public $IntermediarySoftwareInfo;


} // end class RequestHeader
