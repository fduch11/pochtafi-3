<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName AdditionalInformationType
 * @var AdditionalInformationType
 * @xmlDefinition Additional information.
 */
class AdditionalInformationType
	{



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\SpecialMentionCodeType $SpecialMentionCode [optional] Special mentions code, FI Customs code list 0051.
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\StatementDescriptionType $StatementDescription [optional] Special mentions descriping text.
		@param fi\tulli\schema\external\common\dme\v1_0\udt\IndicatorType $EUExportIndicator [optional] Flag indicating export from EC.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType $ExportingCountryCode [optional] Code of country exported from.
	*/                                                                        
	public function __construct($SpecialMentionCode = null, $StatementDescription = null, $EUExportIndicator = null, $ExportingCountryCode = null)
	{
		$this->SpecialMentionCode = $SpecialMentionCode;
		$this->StatementDescription = $StatementDescription;
		$this->EUExportIndicator = $EUExportIndicator;
		$this->ExportingCountryCode = $ExportingCountryCode;
	}
	
	/**
	 * @Definition Special mentions code, FI Customs code list 0051.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName SpecialMentionCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\SpecialMentionCodeType
	 */
	public $SpecialMentionCode;
	/**
	 * @Definition Special mentions descriping text.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName StatementDescription
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\StatementDescriptionType
	 */
	public $StatementDescription;
	/**
	 * @Definition Flag indicating export from EC.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName EUExportIndicator
	 * @var fi\tulli\schema\external\common\dme\v1_0\udt\IndicatorType
	 */
	public $EUExportIndicator;
	/**
	 * @Definition Code of country exported from.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName ExportingCountryCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType
	 */
	public $ExportingCountryCode;


} // end class AdditionalInformationType
