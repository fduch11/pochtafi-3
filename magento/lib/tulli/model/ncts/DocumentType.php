<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName DocumentType
 * @var DocumentType
 * @xmlDefinition Additional document information.
 */
class DocumentType
	{



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\DocumentTypeCodeType $DocumentTypeCode [optional] Additional document type code, FI Customs code list 0006.
		@param fi\tulli\schema\external\ncts\dme\v1\DocumentIDType $DocumentID [optional] Document identifier.
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\ItemSequenceNumberType $ItemSequenceNumber [optional] Goods item sequence number.
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\SupplementaryInformationType $SupplementaryInformation [optional] Complement of information relating to additional document.
	*/                                                                        
	public function __construct($DocumentTypeCode = null, $DocumentID = null, $ItemSequenceNumber = null, $SupplementaryInformation = null)
	{
		$this->DocumentTypeCode = $DocumentTypeCode;
		$this->DocumentID = $DocumentID;
		$this->ItemSequenceNumber = $ItemSequenceNumber;
		$this->SupplementaryInformation = $SupplementaryInformation;
	}
	
	/**
	 * @Definition Additional document type code, FI Customs code list 0006.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName DocumentTypeCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\DocumentTypeCodeType
	 */
	public $DocumentTypeCode;
	/**
	 * @Definition Document identifier.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName DocumentID
	 * @var fi\tulli\schema\external\ncts\dme\v1\DocumentIDType
	 */
	public $DocumentID;
	/**
	 * @Definition Goods item sequence number.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName ItemSequenceNumber
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\ItemSequenceNumberType
	 */
	public $ItemSequenceNumber;
	/**
	 * @Definition Complement of information relating to additional document.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName SupplementaryInformation
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\SupplementaryInformationType
	 */
	public $SupplementaryInformation;


} // end class DocumentType
