<?php

/* TODO: manual generated file */

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName ItineraryType
 * @var ItineraryType
 * @xmlDefinition The chronological order the routing countries, through which the goods pass through between the original country of dispatch and the final destination country.
 */
class ItineraryType
	{

	/**
		@param array $RoutingCountryCodes Date of declaration.
	*/
	public function __construct($RoutingCountryCodes)
	{
		$this->RoutingCountryCodes = $RoutingCountryCodes;
	}
	
	/**
	 * @Definition Codes of a route country.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName RoutingCountryCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType
	 */
	public $RoutingCountryCodes;


} // end class ItineraryType