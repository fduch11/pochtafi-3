<?php

/**
 * @xmlNamespace 
 * @xmlType CommodityClassificationCodeType
 * @xmlName TariffClassificationType
 * @var TariffClassificationType
 * @xmlDefinition Commodity classification information.
 */
class TariffClassificationType {

	/**
		@param  $CommodityClassificationCode [optional] TARIC commodity classification code.
	*/                                                                        
	public function __construct($CommodityClassificationCode = null)
	{
		$this->CommodityClassificationCode = $CommodityClassificationCode;
	}
	
	/**
	 * @Definition TARIC commodity classification code.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName CommodityClassificationCode
	 */
	public $CommodityClassificationCode;


} // end class TariffClassificationType
