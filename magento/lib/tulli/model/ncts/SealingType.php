<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName SealingType
 * @var SealingType
 * @xmlDefinition Seals information.
 */
class SealingType
	{



	/**                                                                       
		@param fi\tulli\schema\external\ncts\dme\v1\SealQuantityType $SealQuantity [optional] Seal quantity.
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\SealIDType $SealID [optional] Seal identifier.
	*/                                                                        
	public function __construct($SealQuantity = null, $SealID = null)
	{
		$this->SealQuantity = $SealQuantity;
		$this->SealID = $SealID;
	}
	
	/**
	 * @Definition Seal quantity.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName SealQuantity
	 * @var fi\tulli\schema\external\ncts\dme\v1\SealQuantityType
	 */
	public $SealQuantity;
	/**
	 * @Definition Seal identifier.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlMaxOccurs 99
	 * @xmlName SealID
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\SealIDType
	 */
	public $SealID;


} // end class SealingType
