<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName PartyType
 * @var PartyType
 * @xmlDefinition Trader party information.
 */
class PartyType
	{



	/**                                                                       
		@param fi\tulli\schema\external\ncts\dme\v1\TraderIDType $ID [optional] Trader reference
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\IDExtensionType $IDExtension [optional] Extension of the trader identifier, issued by FI Customs.
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\NameType $Name [optional] Trader name.
		@param fi\tulli\schema\external\ncts\dme\v1\AddressType $Address [optional] Trader address details.
	*/                                                                        
	public function __construct($ID = null, $IDExtension = null, $Name = null, $Address = null)
	{
		$this->ID = $ID;
		$this->IDExtension = $IDExtension;
		$this->Name = $Name;
		$this->Address = $Address;
	}
	
	/**
	 * @Definition Trader reference
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName ID
	 * @var fi\tulli\schema\external\ncts\dme\v1\TraderIDType
	 */
	public $ID;
	/**
	 * @Definition Extension of the trader identifier, issued by FI Customs.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName IDExtension
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\IDExtensionType
	 */
	public $IDExtension;
	/**
	 * @Definition Trader name.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Name
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\NameType
	 */
	public $Name;
	/**
	 * @Definition Trader address details.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Address
	 * @var fi\tulli\schema\external\ncts\dme\v1\AddressType
	 */
	public $Address;


} // end class PartyType
