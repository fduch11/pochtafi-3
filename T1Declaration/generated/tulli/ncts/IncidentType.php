<?php

/* TODO: manual generated file */

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName Incident
 * @var IncidentType
 * @xmlDefinition En route event incident details.
 */
class IncidentType
{

	public function __construct($IncidentIndicator, $Description, $Endorsement)
	{
		$this->IncidentIndicator = $IncidentIndicator;
        $this->Description = $Description;
        $this->Endorsement = $Endorsement;
	}
	
	/**
	 * @Definition Idicator for reporting an incident.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName IncidentIndicator
	 * @var fi\tulli\schema\external\common\dme\v1_0\udt\IndicatorType
	 */
	public $IncidentIndicator;

    /**
     * @Definition Incident descriping text.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlName Description
     * @var fi\tulli\schema\external\common\dme\v1_0\qdt\DescriptionType
     */
    public $Description;

    /**
     * @Definition Endorsement of the reportable event that an authority has confirmed.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlName Endorsement
     * @var fi\tulli\schema\external\common\dme\v1_0\ncts\EndorsementType
     */
    public $Endorsement;

} // end class IncidentType
