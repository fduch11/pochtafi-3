<?php
/**
 * @xmlNamespace 
 * @xmlType TransportMeansType
 * @xmlName ReportType
 * @var ReportType
 */
class ReportType {



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\MovementReferenceIDType $MovementReferenceID [optional] Movement reference number (MRN), issued by Customs.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\TransitTypeCodeType $TransitTypeCode [optional] Transit declaration type code, FI Customs code list 0081.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\FunctionCodeType $FunctionCode [optional] Mesage function code, FI Customs code list 0093.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType $DispatchCountryCode [optional] Country of dispatch.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType $DestinationCountryCode [optional] Destination country.
		@param fi\tulli\schema\external\ncts\dme\v1\CustomsOfficeContactType $TransitDepartureOffice [optional] Customs office where transit procedure starts.
		@param fi\tulli\schema\external\ncts\dme\v1\CustomsOfficeContactType $TransitPresentationOffice [optional] Customs office of presentation.
		@param  $UnloadingRemarks [optional] Unloading remarks information.
		@param  $DepartureTransportMeans [optional] Information on the means of transport on which the goods are directly loaded at the time of transit formalities.
		@param fi\tulli\schema\external\common\dme\v1_0\udt\IndicatorType $ContainerTransportIndicator [optional] Indicates, wether the goods are or are presumed to be transported in container(s) when crossing the Community border.
		@param fi\tulli\schema\external\ncts\dme\v1\SealingType $Sealing [optional] Seals information.
		@param fi\tulli\schema\external\ncts\dme\v1\GoodsItemQuantityType $GoodsItemQuantity [optional] Goods item quantity.
		@param fi\tulli\schema\external\ncts\dme\v1\TotalPackageQuantityType $TotalPackageQuantity [optional] Total package quantity.
		@param fi\tulli\schema\external\ncts\dme\v1\WeightMeasureType $TotalGrossMassMeasure [optional] Total gross mass for all goods items in transit declaration.
		@param  $Consignor [optional] Consignor party.
		@param  $Consignee [optional] Consignee party.
		@param  $AuthorisedConsignee [optional] Authorised consignee party.
		@param  $ActualAuthorisedConsignee [optional] Actual authorised consignee party.
		@param  $Principal [optional] Principal trader party.
	*/                                                                        
	public function __construct($MovementReferenceID = null, $TransitTypeCode = null, $FunctionCode = null, $DispatchCountryCode = null, $DestinationCountryCode = null, $TransitDepartureOffice = null, $TransitPresentationOffice = null, $UnloadingRemarks = null, $DepartureTransportMeans = null, $ContainerTransportIndicator = null, $Sealing = null, $GoodsItemQuantity = null, $TotalPackageQuantity = null, $TotalGrossMassMeasure = null, $Consignor = null, $Consignee = null, $AuthorisedConsignee = null, $ActualAuthorisedConsignee = null, $Principal = null)
	{
		$this->MovementReferenceID = $MovementReferenceID;
		$this->TransitTypeCode = $TransitTypeCode;
		$this->FunctionCode = $FunctionCode;
		$this->DispatchCountryCode = $DispatchCountryCode;
		$this->DestinationCountryCode = $DestinationCountryCode;
		$this->TransitDepartureOffice = $TransitDepartureOffice;
		$this->TransitPresentationOffice = $TransitPresentationOffice;
		$this->UnloadingRemarks = $UnloadingRemarks;
		$this->DepartureTransportMeans = $DepartureTransportMeans;
		$this->ContainerTransportIndicator = $ContainerTransportIndicator;
		$this->Sealing = $Sealing;
		$this->GoodsItemQuantity = $GoodsItemQuantity;
		$this->TotalPackageQuantity = $TotalPackageQuantity;
		$this->TotalGrossMassMeasure = $TotalGrossMassMeasure;
		$this->Consignor = $Consignor;
		$this->Consignee = $Consignee;
		$this->AuthorisedConsignee = $AuthorisedConsignee;
		$this->ActualAuthorisedConsignee = $ActualAuthorisedConsignee;
		$this->Principal = $Principal;
	}
	
	/**
	 * @Definition Movement reference number (MRN), issued by Customs.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName MovementReferenceID
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\MovementReferenceIDType
	 */
	public $MovementReferenceID;
	/**
	 * @Definition Transit declaration type code, FI Customs code list 0081.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName TransitTypeCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\TransitTypeCodeType
	 */
	public $TransitTypeCode;
	/**
	 * @Definition Mesage function code, FI Customs code list 0093.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName FunctionCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\FunctionCodeType
	 */
	public $FunctionCode;
	/**
	 * @Definition Country of dispatch.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName DispatchCountryCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType
	 */
	public $DispatchCountryCode;
	/**
	 * @Definition Destination country.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName DestinationCountryCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType
	 */
	public $DestinationCountryCode;
	/**
	 * @Definition Customs office where transit procedure starts.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName TransitDepartureOffice
	 * @var fi\tulli\schema\external\ncts\dme\v1\CustomsOfficeContactType
	 */
	public $TransitDepartureOffice;
	/**
	 * @Definition Customs office of presentation.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName TransitPresentationOffice
	 * @var fi\tulli\schema\external\ncts\dme\v1\CustomsOfficeContactType
	 */
	public $TransitPresentationOffice;
	/**
	 * @Definition Unloading remarks information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName UnloadingRemarks
	 */
	public $UnloadingRemarks;
	/**
	 * @Definition Information on the means of transport on which the goods are directly loaded at the time of transit formalities.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName DepartureTransportMeans
	 */
	public $DepartureTransportMeans;
	/**
	 * @Definition Indicates, wether the goods are or are presumed to be transported in container(s) when crossing the Community border.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName ContainerTransportIndicator
	 * @var fi\tulli\schema\external\common\dme\v1_0\udt\IndicatorType
	 */
	public $ContainerTransportIndicator;
	/**
	 * @Definition Seals information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Sealing
	 * @var fi\tulli\schema\external\ncts\dme\v1\SealingType
	 */
	public $Sealing;
	/**
	 * @Definition Goods item quantity.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName GoodsItemQuantity
	 * @var fi\tulli\schema\external\ncts\dme\v1\GoodsItemQuantityType
	 */
	public $GoodsItemQuantity;
	/**
	 * @Definition Total package quantity.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName TotalPackageQuantity
	 * @var fi\tulli\schema\external\ncts\dme\v1\TotalPackageQuantityType
	 */
	public $TotalPackageQuantity;
	/**
	 * @Definition Total gross mass for all goods items in transit declaration.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName TotalGrossMassMeasure
	 * @var fi\tulli\schema\external\ncts\dme\v1\WeightMeasureType
	 */
	public $TotalGrossMassMeasure;
	/**
	 * @Definition Consignor party.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Consignor
	 */
	public $Consignor;
	/**
	 * @Definition Consignee party.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Consignee
	 */
	public $Consignee;
	/**
	 * @Definition Authorised consignee party.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName AuthorisedConsignee
	 */
	public $AuthorisedConsignee;
	/**
	 * @Definition Actual authorised consignee party.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName ActualAuthorisedConsignee
	 */
	public $ActualAuthorisedConsignee;
	/**
	 * @Definition Principal trader party.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName Principal
	 */
	public $Principal;


} // end class ReportType
