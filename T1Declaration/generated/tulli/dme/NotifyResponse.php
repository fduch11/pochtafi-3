<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName NotifyResponse
 * @var NotifyResponse
 * @xmlDefinition Response to a notification request.
 */
class NotifyResponse
	{



	/**                                                                       
		@param  $ResponseCode [optional] Status code for notification handling result. The code values are defined in a separate document.
		@param  $ResponseText [optional] Textual description of the status code.
	*/                                                                        
	public function __construct($ResponseHeader = null, $ResponseCode = null, $ResponseText = null)
	{
		$this->ResponseHeader = $ResponseHeader;
		$this->ResponseCode = $ResponseCode;
		$this->ResponseText = $ResponseText;
	}
	
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/notificationservicetypes/v1
	 * @xmlName ResponseHeader
	 * @var fi\tulli\ws\notificationservicetypes\v1\NotifyResponseHeader
	 */
	public $ResponseHeader;
	/**
	 * @Definition Status code for notification handling result. The code values are defined in a separate document.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/notificationservicetypes/v1
	 * @xmlName ResponseCode
	 */
	public $ResponseCode;
	/**
	 * @Definition Textual description of the status code.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/notificationservicetypes/v1
	 * @xmlName ResponseText
	 */
	public $ResponseText;


} // end class NotifyResponse
