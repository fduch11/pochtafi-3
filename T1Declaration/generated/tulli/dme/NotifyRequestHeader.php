<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName NotifyRequestHeader
 * @var NotifyRequestHeader
 * @xmlDefinition Header containing basic metadata for a notification request.
 */
class NotifyRequestHeader
	{



	/**                                                                       
		@param fi\tulli\schema\corporateservice\v1\BusinessId $IntermediaryBusinessId [optional] The business identity code of the message intermediary. The intermediary must be authorized to download from Customs the message to which the notification refers.
		@param string $Timestamp [optional] Indicates when the notification request was sent from Customs to the intermediary.
		@param  $Environment [optional] Indicates whether the notification was sent from a test or production environment.
	*/                                                                        
	public function __construct($IntermediaryBusinessId = null, $TransactionId = null, $Timestamp = null, $Environment = null)
	{
		$this->IntermediaryBusinessId = $IntermediaryBusinessId;
		$this->TransactionId = $TransactionId;
		$this->Timestamp = $Timestamp;
		$this->Environment = $Environment;
	}
	
	/**
	 * @Definition The business identity code of the message intermediary. The intermediary must be authorized to download from Customs the message to which the notification refers.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/notificationservicetypes/v1
	 * @xmlName IntermediaryBusinessId
	 * @var fi\tulli\schema\corporateservice\v1\BusinessId
	 */
	public $IntermediaryBusinessId;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/notificationservicetypes/v1
	 * @xmlName TransactionId
	 * @var fi\tulli\ws\corporateservicetypes\v1\TransactionId
	 */
	public $TransactionId;
	/**
	 * @Definition Indicates when the notification request was sent from Customs to the intermediary.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/notificationservicetypes/v1
	 * @xmlName Timestamp
	 * @var string
	 */
	public $Timestamp;
	/**
	 * @Definition Indicates whether the notification was sent from a test or production environment.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/notificationservicetypes/v1
	 * @xmlName Environment
	 */
	public $Environment;


} // end class NotifyRequestHeader
