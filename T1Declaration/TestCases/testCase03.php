<?php

$params = array(

    'TraderReferenceID' => 'Tehtävä3-IJKL-123456',

    'DepartureTransportMeans' => array(
        'TransportModeCode' => 3,
        'TransportMeansNationalityCode' => 'FI',
        'TransportMeansID' => 'KCL-123 PYP-754 KCL-123',
        'ConveyanceReferenceID' => null
    ),
//            'BorderTransportMeans' => array('TransportModeCode' => 1, 'TransportMeansNationalityCode' => 'EE', 'TransportMeansID' => 'Eestiship', 'ConveyanceReferenceID' => null),

    'DispatchCountryCode' => null,
    'DestinationCountryCode' => null,

    'Itinerary' => array(
        1 => array('RoutingCountryCode' => 'CH')
    ),

    'TransitDestinationOffice' => 'CH006521',

    'TransitBorderOffice' => array('CH001252'),

    'GoodsItemQuantity' => '4',
    'TotalPackageQuantity' => 145,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => '416.123'
    ),

    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI2628792-7R0001'
    ),
    'Loading' => array(
        'LoadingDateTime' => date('Y-m-d\TH:i:s', strtotime("+2 days 09:00")), // +2 дня 09:00 SV
        'LocationName' => 'SV'
    ),
    'Issue' => array(
        'IssueDate' => date('Y-m-d'), //текущая
        'LocationName' => 'Imatra'
    ),
    'TransitLimitDate' => date('Y-m-d', strtotime("+5 days")), // +5 дней
    'ContainerTransportIndicator' => 'false',
    'Sealing' => array(
        'SealQuantity' => 1000,
        'SealID' => 'HE046-E-E047-0001045'
    ),
    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'US',
            'DestinationCountryCode' => 'CH',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Canon Virginia, Inc',
                'Address' => array(
                    'Line' => '12000 Cannon Boulvard',
                    'PostcodeID' => 'VA 23606',
                    'CityName' => 'Newport News',
                    'CountryCode' => 'US'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Canon Virginia, Inc',
                'Address' => array(
                    'Line' => '12000 Cannon Boulvard',
                    'PostcodeID' => 'VA 23606',
                    'CityName' => 'Newport News',
                    'CountryCode' => 'US'
                ),
            ),
            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Cannon AG',
                'Address' => array(
                    'Line' => 'St. Jacobs-Strasse 110',
                    'PostcodeID' => '4132',
                    'CityName' => 'Muttenz',
                    'CountryCode' => 'CH'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Cannon AG',
                'Address' => array(
                    'Line' => 'St. Jacobs-Strasse 110',
                    'PostcodeID' => '4132',
                    'CityName' => 'Muttenz',
                    'CountryCode' => 'CH'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => array(900600),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Kameroita',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 16
            ),
            'NetWeightMeasure' => null,
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'BX',
                    'PackagingMarksID' => 'Canon-yx38',
                    'PackageQuantity' => 100,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                1 => array(
                    'DocumentTypeCode' => 71,
                    'DocumentID' => 'ASG-4896'
                )
            ),
            'AdditionalDocument' => null,
            'AdditionalInformation' => null,
            'TransportEquipment' => null,
            'FreightPaymentMethodCode' => null
        ),
        2 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'BD',
            'DestinationCountryCode' => 'IT',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Canon Virginia, Inc',
                'Address' => array(
                    'Line' => '12000 Cannon Boulvard',
                    'PostcodeID' => 'VA 23606',
                    'CityName' => 'Newport News',
                    'CountryCode' => 'US'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Canon Virginia, Inc',
                'Address' => array(
                    'Line' => '12000 Cannon Boulvard',
                    'PostcodeID' => 'VA 23606',
                    'CityName' => 'Newport News',
                    'CountryCode' => 'US'
                ),
            ),
            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Canon Italia S.p.A.',
                'Address' => array(
                    'Line' => 'Via Milano 8',
                    'PostcodeID' => '20097',
                    'CityName' => 'San Donato Milanese',
                    'CountryCode' => 'IT'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Canon Italia S.p.A.',
                'Address' => array(
                    'Line' => 'Via Milano 8',
                    'PostcodeID' => '20097',
                    'CityName' => 'San Donato Milanese',
                    'CountryCode' => 'IT'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => array(900600),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Kameroita',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => '0.123'
            ),
            'NetWeightMeasure' => null,
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'Canon-yx38',
                    'PackageQuantity' => 20,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                1 => array(
                    'DocumentTypeCode' => 71,
                    'DocumentID' => 'ASG-4897'
                )
            ),
            'AdditionalDocument' => null,
            'AdditionalInformation' => null,
            'TransportEquipment' => null,
            'FreightPaymentMethodCode' => null
        ),
        3 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T2',
            'DispatchCountryCode' => 'FI',
            'DestinationCountryCode' => 'CH',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'HiKonepojat Oy',
                'Address' => array(
                    'Line' => 'Teollisuustie 3',
                    'PostcodeID' => '00700',
                    'CityName' => 'Helsinki',
                    'CountryCode' => 'FI'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'HiKonepojat Oy',
                'Address' => array(
                    'Line' => 'Teollisuustie 3',
                    'PostcodeID' => '00700',
                    'CityName' => 'Helsinki',
                    'CountryCode' => 'FI'
                ),
            ),
            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Swedwood AG',
                'Address' => array(
                    'Line' => 'Zehnstrasse 48',
                    'PostcodeID' => '54589',
                    'CityName' => 'Basel',
                    'CountryCode' => 'CH'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Swedwood AG',
                'Address' => array(
                    'Line' => 'Zehnstrasse 48',
                    'PostcodeID' => '54589',
                    'CityName' => 'Basel',
                    'CountryCode' => 'CH'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => array(846593),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Hiomakoneita',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 200
            ),
            'NetWeightMeasure' => null,
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'NE',
                    'PackagingMarksID' => '1-5',
                    'PackageQuantity' => null,
                    'PieceCountQuantity' => 5
                )
            ),
            'PreviousDocument' => array(
                2 => array(
                    'DocumentTypeCode' => 10,
                    'DocumentID' => '14FI000000000332E1',
                    'ItemSequenceNumber' => 2
                )
            ),
            'AdditionalDocument' => null,
            'AdditionalInformation' => array(
                'SpecialMentionCode' => 'DG2',
                'StatementDescription' => null,
                'EUExportIndicator' => null,
                'ExportingCountryCode' => null
            ),
            'TransportEquipment' => null,
            'FreightPaymentMethodCode' => null
        ),
        4 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T2',
            'DispatchCountryCode' => 'FI',
            'DestinationCountryCode' => 'CH',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Canon Oy',
                'Address' => array(
                    'Line' => 'PL 100',
                    'PostcodeID' => '00101',
                    'CityName' => 'Helsinki',
                    'CountryCode' => 'FI'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Canon Oy',
                'Address' => array(
                    'Line' => 'PL 100',
                    'PostcodeID' => '00101',
                    'CityName' => 'Helsinki',
                    'CountryCode' => 'FI'
                ),
            ),
            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Canon Italia S.p.A.',
                'Address' => array(
                    'Line' => 'Via Milano 8',
                    'PostcodeID' => '20097',
                    'CityName' => 'San Donato Milanese',
                    'CountryCode' => 'IT'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Canon Italia S.p.A.',
                'Address' => array(
                    'Line' => 'Via Milano 8',
                    'PostcodeID' => '20097',
                    'CityName' => 'San Donato Milanese',
                    'CountryCode' => 'IT'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => array(900600),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Kameroita',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 200
            ),
            'NetWeightMeasure' => null,
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'BX',
                    'PackagingMarksID' => 'Canon-1879',
                    'PackageQuantity' => 20,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                2 => array(
                    'DocumentTypeCode' => 10,
                    'DocumentID' => '14FI000000000332E1',
                    'ItemSequenceNumber' => 2
                )
            ),
            'AdditionalDocument' => null,
            'AdditionalInformation' => array(
                'SpecialMentionCode' => 'DG2',
                'StatementDescription' => null,
                'EUExportIndicator' => null,
                'ExportingCountryCode' => null
            ),
            'TransportEquipment' => null,
            'FreightPaymentMethodCode' => null
        )
    )
);

?>