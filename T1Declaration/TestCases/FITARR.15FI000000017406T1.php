<?php

$params = array(

    'XMessageType' => 'FITARR',

    'TraderReferenceID' => 'FITDEC20150205D1',

    'MovementReferenceID' => '15FI000000017406T1',
	
    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI2628792-7R0001'
    ),
	'TransitPresentationOffice' => array(
		'CustomsOfficeCode' => 'FI542300'
	),
	'NotificationDate' => '2015-02-09',//date('Y-m-d'), // date('Y-m-d'), //20040304
	'SealConditionIndicator' => 1,      // 'OK',
	'DestinationLanguageCode' => 'FI',

    'ActualAuthorisedConsignee' => array(
        'ID' => 'FI2628792-7',
        'IDExtension' => 'T0001',
        'Name' => 'Pochta.fi Oy',
        'Address' => array(
            'Line' => 'Pelkolankatu 5',
            'PostcodeID' => '53420',
            'CityName' => 'Lappeenranta',
            'CountryCode' => 'FI'
        ),
    )

);

?>