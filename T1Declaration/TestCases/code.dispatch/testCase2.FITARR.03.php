<?php

$params = array(

    'XMessageType' => 'FITARR',

    'TraderReferenceID' => 'Tehtävä2-FITARR',

    'MovementReferenceID' => '14FI000000000439T3',
	
    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI2628792-7R0001'
    ),
	'TransitPresentationOffice' => array(
		'CustomsOfficeCode' => 'FI534200'
	),
	'NotificationDate' => '2014-11-14', //date('Y-m-d', '20040304'),
	'SealConditionIndicator' => 1,      //'OK',
	'DestinationLanguageCode' => 'FI',

    'ActualAuthorisedConsignee' => array(
        'ID' => 'FI2628792-7',
        'IDExtension' => 'T0001',
        'Name' => 'Pochta.fi Oy',
        'Address' => array(
            'Line' => 'Kultakuusenkuja 4',
            'PostcodeID' => '55610',
            'CityName' => 'Imatra',
            'CountryCode' => 'FI'
        )
    ),

	'EnRouteEvent' => array(
		'LocationName' => 'Jyväskylä',
		'CountryCode' => 'FI',
		'TransShipment'	=> array(	
			'Endorsement' => array(
				'EndorsementDate' => '2004-03-04',  //date('Y-m-d', '20040304'),
				'AuthorityDescription' => 'Jyväskylän tulli',
				'AuthorityCountryCode' => 'FI'
			),
			'NewTransportMeans' => array(
				'TransportMeansID' => 'HVU-116/PYP-754',
				'TransportMeansNationalityCode' => 'FI'
			)
		)
	)
	
);

?>