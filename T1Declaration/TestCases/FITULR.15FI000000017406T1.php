<?php

$params = array(

    'XMessageType' => 'FITULR',

    'MovementReferenceID' => '15FI000000017406T1',

    'FunctionCode' => '6',
    'TransitTypeCode' => 'T1',

    'TransitPresentationOffice' => array(
        'CustomsOfficeCode' => 'FI542300'
    ),

    'UnloadingRemarks' => array(
        'RemarksDate' => '2015-02-09',
        'LocationName' => 'FI542300'
    ),

    'GoodsItemQuantity' => 1,
    'TotalPackageQuantity' => 1,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => "0.34"
    ),

    'ActualAuthorisedConsignee' => array(
        'ID' => 'FI2628792-7',
        'IDExtension' => 'T0001',
//        'Name' => 'Pochta.fi Oy',             // not allowed for ActualAuthorisedConsignee
//        'Address' => array(
//            'Line' => 'Kultakuusenkuja 4',
//            'PostcodeID' => '55610',
//            'CityName' => 'Imatra',
//            'CountryCode' => 'FI'
//        ),
    ),

    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'FI',
            'DestinationCountryCode' => 'FI',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'MORADA MUSIC/AUDIO FIDELITY',
                'Address' => array(
                    'Line' => '79 E. DAILU DR., #507',
                    'PostcodeID' => 'CA 93010',
                    'CityName' => 'CAMARILLO',
                    'CountryCode' => 'US'
                ),
            ),

            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Pochta.fi Oy',
                'Address' => array(
                    'Line' => 'Pelkolankatu 5',
                    'PostcodeID' => '53420',
                    'CityName' => 'Lappeenranta',
                    'CountryCode' => 'FI'
                ),
            ),

            'Commodity' => array(
                'TariffClassification' => array(852349),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'CD',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => '0.34'
            ),
//            'NetWeightMeasure' => array(
//                'UnitCode' => 'KGM',
//                'Value' => '29.123'
//            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'LZ168021770US',
                    'PackageQuantity' => 1,
                    'PieceCountQuantity' => null
                )
            ),
//            'PreviousDocument' => array(
//                1 => array(
//                    'DocumentTypeCode' => '740',
//                    'DocumentID' => 'TR-2527'
//                ),
//            ),
            'AdditionalDocument' => array(
                1 => array(
                    'DocumentTypeCode' => '740',
                    'DocumentID' => 'LZ168021770US'
                ),
            ),
            'AdditionalInformation' => null,
//            'TransportEquipment' => array(
//                1 => array('TransportEquipmentID' => 'ABCD1234-5'),
//            ),
            'FreightPaymentMethodCode' => null
        )
    )
);

?>