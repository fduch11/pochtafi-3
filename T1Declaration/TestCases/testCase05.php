<?php

$params = array(

    'TraderReferenceID' => 'Tehtävä5-YZX-Automat',

    'DepartureTransportMeans' => array(
        'TransportModeCode' => 3,
        'TransportMeansNationalityCode' => 'FI',
        'TransportMeansID' => 'RGY-889 PNO-15',
        'ConveyanceReferenceID' => null
    ),
//          'BorderTransportMeans' => array('TransportModeCode' => 1, 'TransportMeansNationalityCode' => 'EE', 'TransportMeansID' => 'Eestiship', 'ConveyanceReferenceID' => null),

    'DispatchCountryCode' => null,
    'DestinationCountryCode' => null,

    'TransitDestinationOffice' => 'SK526700',

    'GoodsItemQuantity' => 1,
    'TotalPackageQuantity' => 2080,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => 1300
    ),

    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI2628792-7R0001'
    ),
    'Loading' => array(
        'LoadingDateTime' => date('Y-m-d\TH:i:s', strtotime("today 11:15")), // текущая, 11:15 FI
        'LocationName' => 'FI'
    ),
    'Issue' => array(
        'IssueDate' => date('Y-m-d'), //текущая
        'LocationName' => 'Imatra'
    ),
    'TransitLimitDate' => date('Y-m-d', strtotime("+3 days")), // +3 дней
    'ContainerTransportIndicator' => 'true',
    'Sealing' => array(
        'SealQuantity' => 1,
        'SealID' => 'HE123-1'
    ),
    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'MD',
            'DestinationCountryCode' => 'SK',
            'Consignor' => array(
                'ID' => 'FI2628792-7',
                'IDExtension' => 'T0001',
                'Name' => 'Pochta.fi Oy',
                'Address' => array(
                    'Line' => 'Kultakuusenkuja 4',
                    'PostcodeID' => '55610',
                    'CityName' => 'Imatra',
                    'CountryCode' => 'FI'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => 'FI2628792-7',
                'IDExtension' => 'T0001',
                'Name' => 'Pochta.fi Oy',
                'Address' => array(
                    'Line' => 'Kultakuusenkuja 4',
                    'PostcodeID' => '55610',
                    'CityName' => 'Imatra',
                    'CountryCode' => 'FI'
                ),
            ),
            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Automat V.K.M',
                'Address' => array(
                    'Line' => 'Ursinyho 2',
                    'PostcodeID' => '83102',
                    'CityName' => 'Bratislava',
                    'CountryCode' => 'SK'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Automat V.K.M',
                'Address' => array(
                    'Line' => 'Ursinyho 2',
                    'PostcodeID' => '83102',
                    'CityName' => 'Bratislava',
                    'CountryCode' => 'SK'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => array(630499),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Karvanoppia',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 1300
            ),
            'NetWeightMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 1290
            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'KARVIS',
                    'PackageQuantity' => 2080,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                1 => array(
                    'DocumentTypeCode' => 'SUM',
                    'DocumentID' => '14FI000000000788I0',
                    'ItemSequenceNumber' => 1
                )
            ),

            'AdditionalDocument' => array(
                1 => array(
                    'DocumentTypeCode' => '271',
                    'DocumentID' => '2/07',
                    'SupplementaryInformation' => null
                ),
                2 => array(
                    'DocumentTypeCode' => '380',
                    'DocumentID' => '2/07',
                    'SupplementaryInformation' => null
                )
            ),
            'AdditionalInformation' => null,
            'TransportEquipment' => array(
                1 => array('TransportEquipmentID' => 'LLKK9876-1'),
            ),
            'FreightPaymentMethodCode' => null
        )
    )
);

?>